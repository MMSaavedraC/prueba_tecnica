from django.db import models

class Weather(models.Model):
    _id = models.CharField(max_length=100)
    cityid = models.CharField(max_length=100)
    validdateutc = models.CharField(max_length=100)
    winddirectioncardinal = models.CharField(max_length=100)
    probabilityofprecip = models.CharField(max_length=100)
    relativehumidity = models.CharField(max_length=100)
    name = models.CharField(max_length=100)
    #date_insert = models.DateTimeField(blank=True, null=True)
    longitude = models.FloatField(blank=True, null=True)
    state = models.CharField(max_length=100)
    #lastreporttime = models.DateTimeField(blank=True, null=True)
    skydescriptionlong = models.CharField(max_length=100)
    stateabbr = models.CharField(max_length=100)
    tempc = models.IntegerField(blank=True, null=True)
    latitude = models.FloatField(blank=True, null=True)
    iconcode = models.IntegerField(blank=True, null=True)
    windspeedkm = models.IntegerField(blank=True, null=True)

    def __str__(self):
        return self.name


