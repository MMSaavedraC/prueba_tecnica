# Generated by Django 2.1 on 2019-04-15 09:52

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Weather',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('_id', models.CharField(max_length=100)),
                ('cityid', models.CharField(max_length=100)),
                ('validdateutc', models.CharField(max_length=100)),
                ('winddirectioncardinal', models.CharField(max_length=100)),
                ('probabilityofprecip', models.CharField(max_length=100)),
                ('relativehumidity', models.CharField(max_length=100)),
                ('name', models.CharField(max_length=100)),
                ('longitude', models.FloatField(blank=True, null=True)),
                ('state', models.CharField(max_length=100)),
                ('skydescriptionlong', models.CharField(max_length=100)),
                ('stateabbr', models.CharField(max_length=100)),
                ('tempc', models.IntegerField(blank=True, null=True)),
                ('latitude', models.FloatField(blank=True, null=True)),
                ('iconcode', models.IntegerField(blank=True, null=True)),
                ('windspeedkm', models.IntegerField(blank=True, null=True)),
            ],
        ),
    ]
