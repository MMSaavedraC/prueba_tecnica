from dateutil.parser import parse
import json
import requests

from django.shortcuts import render
from django.http import HttpResponse

from .models import Weather


def load_data(request):
    url = 'https://api.datos.gob.mx/v1/condiciones-atmosfericas'
    response = requests.request("GET", url)
    raw_data = json.loads(response.text)


    for record in raw_data['results']:
        w, created = Weather.objects.get_or_create(_id=record['_id'])
        w.cityid = record['cityid']
        w.validdateutc = record['validdateutc']
        w.winddirectioncardinal = record['winddirectioncardinal']
        w.probabilityofprecip = record['probabilityofprecip']
        w.relativehumidity = record['relativehumidity']
        w.name = record['name']
        #w.date_insert = parse(record['date_insert'])
        w.longitude = float(record['longitude'])
        w.state = record['state']
        #w.lastreporttime = parse(record['lastreporttime'])
        w.skydescriptionlong = record['skydescriptionlong']
        w.stateabbr = record['stateabbr']
        w.tempc = record['tempc']
        w.latitude = record['latitude']
        w.iconcode = record['iconcode']
        w.windspeedkm = record['windspeedkm']

        w.save()

        '-1.25'



    return HttpResponse("Database was succesfully updated.")
