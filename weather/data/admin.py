from django.contrib import admin

from .models import Weather

class WeatherAdmin(admin.ModelAdmin):
    list_display = ['name', 'cityid']
    list_filter = ['cityid']
    search_fields = ['name']

admin.site.register(Weather, WeatherAdmin)
